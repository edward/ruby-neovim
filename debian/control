Source: ruby-neovim
Section: ruby
Priority: optional
Maintainer: Debian Ruby Team <pkg-ruby-extras-maintainers@lists.alioth.debian.org>
Uploaders: Jason Pleau <jason@jpleau.ca>
Build-Depends: debhelper-compat (= 12),
               gem2deb,
               neovim,
               rake,
               ruby-msgpack (>= 1.0),
               ruby-rspec,
               ruby-multi-json
Standards-Version: 4.4.1
Vcs-Git: https://salsa.debian.org/ruby-team/ruby-neovim.git
Vcs-Browser: https://salsa.debian.org/ruby-team/ruby-neovim
Homepage: https://github.com/alexgenco/neovim-ruby
Testsuite: autopkgtest-pkg-ruby
XS-Ruby-Versions: all

Package: ruby-neovim
Architecture: all
XB-Ruby-Versions: ${ruby:Versions}
Depends: ruby | ruby-interpreter,
         ruby-msgpack (>= 1.0),
         ruby-multi-json,
         neovim,
         ${misc:Depends}
Description: Ruby client for Neovim
 Ruby library to control remote neovim instances.
 .
 Can be used as a neovim plugin host, allowing one to write neovim plugins
 completely in ruby.
 .
 It also acts as a compatibility layer for Ruby plugins written for legacy
 vim. The :ruby, :rubyfile, and :rubydo commands are intended to behave the
 same as they did in vim.
